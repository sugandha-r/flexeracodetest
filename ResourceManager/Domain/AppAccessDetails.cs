﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ResourceManager.Domain
{
    public class AppAccessDetails
    {
        public int ComputerId { get; set; }
        public int UserId { get; set; }
        public int ApplicationId { get; set; }
        public ComputerType ComputerType { get; set; }
        public string Comment { get; set; }
    }

    public enum ComputerType
    {
        None = 0,
        Desktop = 1,
        Laptop = 2
    }
}
