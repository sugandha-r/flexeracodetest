﻿using ResourceManager.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ResourceManager.Processors
{
    public class ApplicationProcessor: BaseProcessor
    {
        public ApplicationProcessor(int appIdC, IList<AppAccessDetails> appAccessData) : base(appIdC, appAccessData)
        {
        }

        public override int CalculateMinResourcesRequired()
        {
            var resourceReqd = default(int);
            foreach (var item in AppAccessData.Where(x => x.ApplicationId == ApplicationId && x.ComputerType != ComputerType.None).GroupBy(x => new { x.UserId }))
            {
                var uniqueItems = item.GroupBy(x => new { x.ComputerId, x.ComputerType });
                var countComputers = uniqueItems.Count(x => x.Key.ComputerType == ComputerType.Desktop);
                var countLaptops = uniqueItems.Count(x => x.Key.ComputerType == ComputerType.Laptop);

                resourceReqd += CalculateResourcePerUser(countComputers, countLaptops);
            }

            return resourceReqd;
        }

        private int CalculateResourcePerUser(int countComp, int countLaptops)
        {
            if (countComp >= countLaptops)
            {
                return countComp;
            }
            else
            {
                // computer < laptop
                return countComp + (int)Math.Ceiling((decimal)(countLaptops - countComp) / 2);
            }
        }
    }
}
