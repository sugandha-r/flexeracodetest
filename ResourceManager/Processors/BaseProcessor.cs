﻿using ResourceManager.Domain;
using System.Collections.Generic;

namespace ResourceManager.Processors
{
    public abstract class BaseProcessor
    {
        public int ApplicationId { get; }
        public IList<AppAccessDetails> AppAccessData { get; }

        public BaseProcessor(int applicationId, IList<AppAccessDetails> appAccessData)
        {
            ApplicationId = applicationId; 
            AppAccessData = appAccessData;
        }

        public abstract int CalculateMinResourcesRequired();
    }
}
