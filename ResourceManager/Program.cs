﻿using Microsoft.Extensions.Configuration;
using ResourceManager.Processors;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;

namespace ResourceManager
{
    class Program
    {
        static void Main(string[] args)
        {          
            var resourceRequired = Process();

            Console.WriteLine(string.Format("Number of minimum resources required for Application Id = {0}", resourceRequired));
            Console.ReadLine();
        }

        static int Process()
        {
            var filePath = GetPath();
            if (!string.IsNullOrWhiteSpace(filePath))
            {
                DataAccessor dataAccessor = new DataAccessor();
                var data = dataAccessor.ReadFromFile(filePath);
                var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

                IConfiguration configuration = builder.Build();

                var appId = configuration.GetSection("Applications:CustomAppId").Value;
                if(!int.TryParse(appId, out int result))
                {
                    result = 374; // Setting default
                }
                ApplicationProcessor processor = new ApplicationProcessor(result, data); // setup DI for resolving different processors as required.
                return processor.CalculateMinResourcesRequired();
            }
            return default;
        }

        static string GetPath()
        {
            string filePath;
            Console.WriteLine(@"Enter 1 for accessing small sample data, 2 for larger sample data, 3 to enter path to new file in accepted format.");
            var a = Console.ReadLine();
            if (a == "1")
            {
                filePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @".\sample-small.csv";
            }
            else if (a == "2")
            {
                filePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @".\sample-large.csv";
            }
            else if (a == "3")
            {
                Console.WriteLine(@"The accepted format is - The file should be comma separated (,) and data starting from 2nd row as - ComputerID,UserID,ApplicationID,ComputerType,Comment");
                Console.WriteLine(@"Please enter the filepath");
                filePath = Console.ReadLine(); 
            }
            else
            {
                Console.WriteLine("Incorrect Response, please try again.");
                filePath = string.Empty;
            }
            return filePath;
        }
    }
}
