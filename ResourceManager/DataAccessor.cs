﻿using ResourceManager.Domain;
using System;
using System.Collections.Generic;
using System.IO;

namespace ResourceManager
{
    public class DataAccessor
    {
        public IList<AppAccessDetails> ReadFromFile(string filePath)
        {
            IList<AppAccessDetails> appAccessData = null;
            if (File.Exists(filePath))
            {
                appAccessData = new List<AppAccessDetails>();
                using (var reader = new StreamReader(filePath))
                {
                    reader.ReadLine(); // Skip the column name line.
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        var values = line.Split(','); // todo - move to config

                        var row = new AppAccessDetails
                        {
                            ComputerId = ConvertToInt(values[0]),
                            UserId = ConvertToInt(values[1]),
                            ApplicationId = ConvertToInt(values[2]),
                            ComputerType = ConvertToEnum(values[3]),
                            Comment = values[4]
                        };

                        appAccessData.Add(row);
                    }
                }
            }

            return appAccessData;
        }

        private int ConvertToInt(string str)
        {
            _ = int.TryParse(str, out int result);
            return result;
        }

        private ComputerType ConvertToEnum(string str)
        {
            Enum.TryParse(str, true, out ComputerType computerType);
            return computerType;
        }
    }
}
