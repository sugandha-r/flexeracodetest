using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ResourceManager.Domain;
using ResourceManager.Processors;

namespace ResourceManagerTest
{
    [TestClass]
    public class ApplicationProcessorTest
    {
        private IList<AppAccessDetails> mockData;
        
        [TestInitialize]
        public void Setup()
        {
            // Runs before each test. 
        }

        [TestMethod]
        public void Calculate_LaptopComputerCountEqual()
        {
            mockData = new List<AppAccessDetails>
            {
                new AppAccessDetails
                {
                    ApplicationId = 374,
                    ComputerId = 1,
                    UserId = 1,
                    ComputerType = ComputerType.Desktop,
                    Comment = "Exported from System A"
                },
                new AppAccessDetails
                {
                    ApplicationId = 374,
                    ComputerId = 2,
                    UserId = 1,
                    ComputerType = ComputerType.Laptop,
                    Comment = "Exported from System A"
                }
            };
            var expectedResult = 1;
            var processor = new ApplicationProcessor(374, mockData);
            var actual = processor.CalculateMinResourcesRequired();
            Assert.AreEqual(expectedResult, actual);
        }


        [TestMethod]
        public void Calculate_DesktopMoreThanLaptop()
        {
            mockData = new List<AppAccessDetails>
            {
                new AppAccessDetails
                {
                    ApplicationId = 374,
                    ComputerId = 1,
                    UserId = 1,
                    ComputerType = ComputerType.Desktop,
                    Comment = "Exported from System A"
                },
                new AppAccessDetails
                {
                    ApplicationId = 374,
                    ComputerId = 2,
                    UserId = 1,
                    ComputerType = ComputerType.Desktop,
                    Comment = "Exported from System A"
                },
                new AppAccessDetails
                {
                    ApplicationId = 374,
                    ComputerId = 2,
                    UserId = 1,
                    ComputerType = ComputerType.Laptop,
                    Comment = "Exported from System A"
                }
            };
            var expectedResult = 2;
            var processor = new ApplicationProcessor(374, mockData);
            var actual = processor.CalculateMinResourcesRequired();
            Assert.AreEqual(expectedResult, actual);
        }

        [TestMethod]
        public void Calculate_DesktopDuplicateRecords()
        {
            mockData = new List<AppAccessDetails>
            {
                new AppAccessDetails
                {
                    ApplicationId = 374,
                    ComputerId = 1,
                    UserId = 1,
                    ComputerType = ComputerType.Desktop,
                    Comment = "Exported from System A"
                },
                new AppAccessDetails
                {
                    ApplicationId = 374,
                    ComputerId = 1,
                    UserId = 1,
                    ComputerType = ComputerType.Desktop,
                    Comment = "Exported from System B"
                }
            };
            var expectedResult = 1;
            var processor = new ApplicationProcessor(374, mockData);
            var actual = processor.CalculateMinResourcesRequired();
            Assert.AreEqual(expectedResult, actual);
        }

        [TestMethod]
        public void Calculate_LaptopDuplicateRecords()
        {
            mockData = new List<AppAccessDetails>
            {
                new AppAccessDetails
                {
                    ApplicationId = 374,
                    ComputerId = 1,
                    UserId = 1,
                    ComputerType = ComputerType.Laptop,
                    Comment = "Exported from System A"
                },
                new AppAccessDetails
                {
                    ApplicationId = 374,
                    ComputerId = 1,
                    UserId = 1,
                    ComputerType = ComputerType.Laptop,
                    Comment = "Exported from System B"
                }
            };
            var expectedResult = 1;
            var processor = new ApplicationProcessor(374, mockData);
            var actual = processor.CalculateMinResourcesRequired();
            Assert.AreEqual(expectedResult, actual);
        }


        [TestMethod]
        public void Calculate_LaptopMoreThanDesktop()
        {
            mockData = new List<AppAccessDetails>
            {
                new AppAccessDetails
                {
                    ApplicationId = 374,
                    ComputerId = 1,
                    UserId = 1,
                    ComputerType = ComputerType.Laptop,
                    Comment = "Exported from System A"
                },
                new AppAccessDetails
                {
                    ApplicationId = 374,
                    ComputerId = 2,
                    UserId = 1,
                    ComputerType = ComputerType.Laptop,
                    Comment = "Exported from System A"
                },
                new AppAccessDetails
                {
                    ApplicationId = 374,
                    ComputerId = 2,
                    UserId = 1,
                    ComputerType = ComputerType.Desktop,
                    Comment = "Exported from System A"
                }
            };
            var expectedResult = 2;
            var processor = new ApplicationProcessor(374, mockData);
            var actual = processor.CalculateMinResourcesRequired();
            Assert.AreEqual(expectedResult, actual);
        }

        //IList<AppAccessDetails> appAccessData = new List<AppAccessDetails>();
        //appAccessData.Add(new AppAccessDetails
        //{
        //    ApplicationId = 364,
        //    Comment = "test",
        //    ComputerId = 1,
        //    ComputerType = ComputerType.Desktop,
        //    UserId = 1
        //});
        //appAccessData.Add(new AppAccessDetails
        //{
        //    ApplicationId = 364,
        //    Comment = "test",
        //    ComputerId = 1,
        //    ComputerType = ComputerType.Desktop,
        //    UserId = 1
        //});
        //appAccessData.Add(new AppAccessDetails
        //{
        //    ApplicationId = 364,
        //    Comment = "test",
        //    ComputerId = 1,
        //    ComputerType = ComputerType.Laptop,
        //    UserId = 1
        //});
        //appAccessData.Add(new AppAccessDetails
        //{
        //    ApplicationId = 364,
        //    Comment = "test",
        //    ComputerId = 1,
        //    ComputerType = ComputerType.Laptop,
        //    UserId = 1
        //});
        //appAccessData.Add(new AppAccessDetails
        //{
        //    ApplicationId = 364,
        //    Comment = "test",
        //    ComputerId = 1,
        //    ComputerType = ComputerType.Laptop,
        //    UserId = 1
        //});

        //ApplicationProcessor p = new ApplicationProcessor("e", appAccessData);
        //p.CalculateMinResourcesRequired();
    }
}
